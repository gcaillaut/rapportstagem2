\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Travaux pr\IeC {\'e}c\IeC {\'e}dents}{4}{subsection.1.1}
\contentsline {section}{\numberline {2}Introduction aux domaines}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Espaces pr\IeC {\'e}topologiques}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}D\IeC {\'e}finition}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Adh\IeC {\'e}rences et ferm\IeC {\'e}s}{4}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Espace pr\IeC {\'e}topologique de type ${\cal V}$}{5}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Apprentissage automatique}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Apprentissage automatique supervis\IeC {\'e}}{5}{subsubsection.2.2.1}
\contentsline {section}{\numberline {3}Mod\IeC {\'e}lisation d'un espace pr\IeC {\'e}topologique}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}DNF positives et espaces pr\IeC {\'e}topologiques de type ${\cal V}$}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Propri\IeC {\'e}t\IeC {\'e}s}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Espaces pr\IeC {\'e}topologiques et apprentissage automatique}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Apprentissage d'une DNF}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}L'apprentissage multi-instances \IeC {\`a} la rescousse}{9}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Apprentissage multi-instances}{10}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}Apprentissage d'un espace pr\IeC {\'e}topologique dans un cadre multi-instances}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Probl\IeC {\`e}me du nombre de sacs}{11}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Repr\IeC {\'e}sentation d'un ferm\IeC {\'e} \IeC {\'e}l\IeC {\'e}mentaire par un treillis}{11}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Nombre de sacs positifs}{12}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Nombre de sacs n\IeC {\'e}gatifs}{12}{subsubsection.5.1.3}
\contentsline {paragraph}{$F(x) = E$}{12}{equation.5.8}
\contentsline {paragraph}{$F(x) \subset E$}{12}{equation.5.9}
\contentsline {subsubsection}{\numberline {5.1.4}D\IeC {\'e}nombrage exact des sacs}{14}{subsubsection.5.1.4}
\contentsline {subsection}{\numberline {5.2}Algorithme}{14}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Induction d'une DNF par CN2}{15}{subsubsection.5.2.1}
\contentsline {subsection}{\numberline {5.3}M\IeC {\'e}triques}{15}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusion}{16}{section.6}
