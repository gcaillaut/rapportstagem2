INPUT=rapport.tex
BIB=rapport
L=pdflatex
OUT_DIR=build

all: $(INPUT)
	if [ ! -d "$(OUT_DIR)" ]; then mkdir "$(OUT_DIR)"; fi
	$(L) --halt-on-error --output-directory "$(OUT_DIR)" "$(INPUT)"
	bibtex $(OUT_DIR)/$(BIB)
	$(L) --halt-on-error --output-directory "$(OUT_DIR)" "$(INPUT)"
	$(L) --halt-on-error --output-directory "$(OUT_DIR)" "$(INPUT)"

clean:
	rm $(OUT_DIR)/*
	rm *~