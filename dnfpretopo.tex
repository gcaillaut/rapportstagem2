\section{Modélisation d'un espace prétopologique}

Il a été décidé que la fonction d'adhérence apprise doit être sous la forme d'une forme normale disjonctive (DNF) positive.
On peut montrer qu'une fonction d'adhérence sous cette forme engendre un espace prétopologique de type ${\cal V}$.

Soit un ensemble d'éléments $E$, un ensemble de relations de voisinages ${\cal N}$ et $A \in {\cal P}(E)$.
Une DNF $Q$ est composée de clauses conjonctives de littéraux $q_{N_i}$ avec $N_i \in {\cal N}$.
$q_{N_i}(A, x)$ est vrai si $x$ est voisin d'un élément de $A$ par $N_i$.
\begin{equation}
  q_{N_i}(A, x) \equiv x \in d_{N_i}^+(A)
\end{equation}

Une clause $c$ est une conjonction de littéraux et est telle que
\begin{equation}
  c(A, x) \equiv \forall q_{N_i} \in c, q_{N_i}(A, x)
\end{equation}

Il est alors possible de créer un prédicat par relation de voisinages, puis de les mettre sous forme d'une DNF $Q$.
$Q$ est alors de la forme $\bigvee_{c \in Q} c(A,x)$ et une clause $c$ est de la forme $\bigwedge_{q \in c} q(A, x)$.
\begin{equation}
    Q(A, x) \equiv \exists c \in Q, c(A, x)
\end{equation}

Soit $Q$ une DNF et $a_Q$ la fonction d'adhérence engendrée par $Q$.
\begin{equation*}
    a_Q(A) = \{ x \in E \mid Q(A, x) \}
\end{equation*} 


\subsection{DNF positives et espaces prétopologiques de type \texorpdfstring{${\cal V}$}{Lg}}

Une DNF positive est composée de littéraux non négatifs. Une fonction d'adhérence construite par une telle DNF
engendre un espace de type ${\cal V}$.

Soit $Q = c_1 \lor \ldots \lor c_k$ avec $c_i$ une conjonction de littéraux positifs.
Soit l'espace prétopologique $(E, a_Q)$ avec $a_Q$ la fonction d'adhérence définie par $Q$.
Soit $A \subseteq B \subseteq {\cal P}(E)$.

Alors $a_Q$ engendre un espace prétopologique de type ${\cal V}$ si et seulement si $a_Q(A) \subseteq a_Q(B)$.

\begin{align*}
    \forall x, x \in a_Q(A) &\Leftrightarrow \exists c \in Q, c(A, x) \\
    &\Leftrightarrow \forall q \in c, q(A, x)
\end{align*}
Or, de part la nature des relations de voisinages utilisées, $\forall q, q(A, x) \Rightarrow q(B, x)$.
\begin{align*}
    \forall x, x \in a_Q(A) &\Leftrightarrow \exists c \in Q, c(A, x) \\
    &\Leftrightarrow \forall q \in c, q(A, x) \\
    &\Rightarrow \forall q \in c, q(B, x) \\
    &\Leftrightarrow \exists c \in Q, c(B, x) \\
    &\Leftrightarrow x \in a_Q(B)  
\end{align*}



\subsection{Propriétés}

Il existe une relation directe entre les opérateurs logiques $\vee$ et
$\wedge$ sur les prédicats $q$ et les opérateurs ensemblistes $\cup$
et $\cap$ sur les fonctions d'adhérence.

\begin{prop}
  $a_{q_1 \land q_2}(A) = a_{q_1}(A) \cap a_{q_2}(A)$
\end{prop}

\begin{proof}
  \begin{equation*}
    \begin{split}
      a_{q_1 \land q_2} &= \{ x \in E \mid q_1(A, x) \land q_2(A, x) \}\\
      &= \{ x \in E \mid q_1(A, x) \} \cap \{ x \in E \mid q_2(A, x) \} \\
      &= a_{q_1}(A) \cap a_{q_2}(A)
    \end{split}
  \end{equation*}
\end{proof}


\begin{prop}
  $a_{q_1 \lor q_2}(A) = a_{q_1}(A) \cup a_{q_2}(A)$
\end{prop}
\begin{proof}

  \begin{equation*}
    \begin{split}
      a_{q_1 \lor q_2} &= \{ x \in E \mid q_1(A, x) \lor q_2(A, x) \}\\
      &= \{ x \in E \mid q_1(A, x) \} \cup \{ x \in E \mid q_2(A, x) \} \\
      &= a_{q_1}(A) \cup a_{q_2}(A)
    \end{split}
  \end{equation*}
\end{proof}

De ces propriétés découlent deux corollaires sur l'isotonie

\begin{cor}
L'isotonie  est stable par réunion. Si $a_{q_1}$ et $a_{q_2}$
engendrent un espace de type V, alors $a_{q_1 \vee q_2}$
engendre un espace de type V.
\end{cor}
\begin{proof}
\begin{align*}
  \forall A, B, A \subseteq B, x \in a_{q_1}(A) \Rightarrow x \in a_{q_1}(B) \\
  \forall A, B, A \subseteq B, x \in a_{q_2}(A) \Rightarrow x \in a_{q_2}(B)
\end{align*}

\begin{align*}
  \forall A, B, A \subseteq B, x \in a_{q_1 \vee q_2}(A)
  &\Rightarrow x \in a_{q_1}(A) \vee x \in a_{q_2}(A) \\
  &\Rightarrow x \in a_{q_1}(B, x) \vee x \in a_{q_2}(B, x) \text{ (car type V)} \\
  &\Rightarrow x \in a_{q_1 \vee q_2}(B)
\end{align*}
\end{proof}

\begin{cor}
L'isotonie est stable par intersection. Si $a_{q_1}$ et $a_{q_2}$
engendrent un espace de type V, alors $a_{q_1 \wedge q_2}$
engendre un espace de type V.
\end{cor}

\begin{proof}
  \begin{align*}
    \forall A, B, A \subseteq B, x \in a_{q_1}(A) \Rightarrow x \in a_{q_1}(B) \\
    \forall A, B, A \subseteq B, x \in a_{q_2}(A) \Rightarrow x \in a_{q_2}(B)
  \end{align*}

  \begin{align*}
    \forall A, B, A \subseteq B, x \in a_{q_1 \wedge q_2}(A)
    &\Rightarrow x \in a_{q_1}(A) \wedge x \in a_{q_2}(A) \\
    &\Rightarrow x \in a_{q_1}(B) \wedge x \in a_{q_2}(B) \text{ (car type V)} \\
    &\Rightarrow x \in a_{q_1 \wedge q_2}(B)
  \end{align*}
\end{proof}

