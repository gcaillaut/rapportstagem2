inotifywait -e modify  . |
while read -r directory events filename; do
  if [ "$filename" =~ ".*\.(tex|bib)$" ]; then
    make
  fi
done
