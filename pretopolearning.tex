\section{Apprentissage d'un espace prétopologique dans un cadre multi-instances}

De nombreux algorithmes d'apprentissage multi-instances ont été développés en modifiant légèrement
les algorithmes d'apprentissage supervisé existant.
Une fois l'ensemble d'apprentissage correctement modifié, on peut alors utiliser l'un de ces algorithmes pour apprendre 
un espace prétopologique.

\subsection{Problème du nombre de sacs}

Bien qu'utiliser les algorithmes existant est possible, cette solution ne semble pas efficace, autant en temps qu'en espace.
En effet, le nombre de sacs générés par un unique élément $x \in E$ est exponentiell en fonction de la taille de $F(x)$. 

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{treillis_simple.png}
    \caption{treillis des éléments de $E = \{camion, vehicule, voiture\}$}
    \label{fig:simpleTreillis}
\end{figure}

\subsubsection{Représentation d'un fermé élémentaire par un treillis}

Soit $(E, a)$ un espace prétopologique de type ${\cal V}$ et $F$ l'opération de fermeture associée.
Soit $x \in E$, on note ${\cal F}_x = \{ x \in A \mid A \subseteq F(x) \}$.
Alors ${\cal F}_x$ est l'ensemble des parties de $E$ incluses dans $F(x)$.

On peut alors déterminer l'ensemble des sacs positifs et négatifs engendrés par $x$.
Pour tout $A \in {\cal F}_x$, on génère un sac positif composé des instances de l'ensemble
$b^+(x) = \{ (y, A) \mid y \in F(x) \setminus A \}$ et un sac négatif composé des instances de l'ensemble 
$b^-(x) = \{ (y, A) \mid y \in E \setminus F(x) \}$.

${\cal F}_x$ est représentable sous la forme d'un treillis (figure \ref{fig:simpleTreillis}). On note ce treillis $T_x^{F(x)}$,
le treillis allant de $x$ à son fermé.
On sait calculer la taille d'un treillis, ${\cal F}_x$ est donc dénombrable.

\begin{align}
    |{\cal F}_x| &= |T_x^{F(x)}| \\
    &= 2^{|F(x)| - 1}
\end{align}

\subsubsection{Nombre de sacs positifs}

Les sacs positifs générés par un élément $x \in E$ sont ceux formés par toutes 
les parties ${\cal F}_x = \{ x \in A \mid A \subset F(x) \}$. Ainsi, un élément $x \in E$ dont le fermé élémentaire est 
lui même n'engendre
pas de sac positif, puisqu'il ne peut pas s'étendre par la fonction d'adhérence. De façon générale, $F(x)$ n'engendre
pas de sac positif, puisque c'est un point fixe.

Le nombre de sacs positifs engendrés par $x \in E$ est alors la taille de l'ensemble 
${\cal F}_x$ moins 1, soitla taille de l'ensemble des parties représentées par le treillis $T_x^{F(x)}$, privé de $F(x)$.
\begin{equation}
    |b^+(x)| = 2^{|F(x)| - 1} - 1
\end{equation}
La taille de l'ensemble des sacs positifs générés par les éléments de $E$ est alors (au pire cas)
\begin{equation}
    \sum_{x \in E} B^+(x)
\end{equation}

La taille de $b^+(x)$ étant exponentielle en fonction de $F(x)$, le nombre de sacs présents dans l'ensemble d'apprentissage 
grandit très vite. Les 
méthodes standards d'apprentissage se révèlent alors très peu efficaces face à ce problème.

\begin{figure}
    \centering
    \includegraphics{nb_sacs_pos.png}
    \caption{Nombre de sacs positifs par rapport à la taille du fermé}
\end{figure}

\subsubsection{Nombre de sacs négatifs}

Pour chaque sac positif $b^+$, un sac négatif $b^-$ lui est associé.
La taille de $b^-$ est relative à celle de $b^+$ et de $E$ ; puisque si une instance $(y, A) \in E \times {\cal F}_x$
appraît dans $b^+$, elle ne peut être présente dans $b^-$. Et si $b^+$ ne contient pas $(y, A)$, alors $b^-$
la contient.

De par ce fait, il y a deux cas à considérer lors du comptage des sacs négatifs engendrés par $x \in E$.

\paragraph{$F(x) = E$}
    \begin{equation}
        |b^-(x)| = 0
    \end{equation}
    Dans ce cas, $x$ n'engendre pas de sacs négatifs, puisque pour toute instance $(y, A) \in E \times {\cal F}_x$, 
    $(y, A)$ apparait dans un sac positif.
    Alors, tous les sacs négatifs sont de taille nulle.

\paragraph{$F(x) \subset E$}
    \begin{equation}
        |b^-(x)| = 2^{|F(x)| - 1}
    \end{equation}
    Dans ce cas, $x$ engendre autant de sacs négatifs que le treillis $T_x^{F(x)}$ possède de parties de $E$.
    Puisque dans ce cas, $\exists (y, A) \in E \times {\cal F}_x$ telle que $(y, A)$ n'apparait dans aucun sac positif 
    de $x$, alors pour tout sac positif
    engendré par $x$, il existe un sac négatif contenant $y$.
    
    De plus, il faut souligner que $F(x)$ engendre lui aussi un sac négatif, mais pas de positif.

\subsubsection{Dénombrage exact des sacs}

Dans le cadre d'un espace prétopologique de type ${\cal V}$, on peut démontrer que pour deux éléments $x \in E$ et 
$y \in E$, les treillis $T_x^{F(x)}$ et $T_y^{F(y)}$  induis par les familles de parties 
${\cal F}_x = \{ A \in {\cal P}(E) \mid A \subseteq F(x) \}$ et ${\cal F}_y = \{ A \in {\cal P}(E) \mid A \subseteq F(y) \}$
possède une intersection non nulle si et seulement si $F(x) = F(y)$.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{treillis_doublon.png}
    \caption{Treillis avec $E = \{ a, b, c, d \}$}
    \label{fig:treillisDoublon}
\end{figure}

Certains sacs peuvent alors être comptés plusieurs fois. 
La figure \ref{fig:treillisDoublon} représente le treillis sur $E = \{ a, b, c, d \}$ tel que $F(a) = F(b) = \{ a, b, c\}$.
On peut clairement distinguer l'intersection entre les treillis $T_a^{F(a)}$ et $T_b^{F(b)}$ : le sous treillis $T_{ab}^{abc}$.

Le nombre de sacs généré peut alors être calculé par la formule
\begin{equation}
    |T_a^{F(a)}| + |T_b^{F(b)}| - |T_{ab}^{abc}|
\end{equation}

De façon générale, on appliquera le \emph{principe d'inclusion-exclusion} pour calculer le nombre de sacs réellement engendrés
par un ensemble $A \in {\cal P}(E)$ tel que $\forall x \in A, F(x) = F_A$ avec $F_A \in {\cal P}(E), A \subseteq F_A$.


\subsection{Algorithme}

Puisque nous somme capable de calculer le nombre de sacs couvert et restant à couvrir (ou à rejetter pour les sacs négatifs),
nous avons pu mettre au point un algorithme ne nécessitant pas d'énumérer l'ensemble des sacs de l'ensemble d'apprentissage.

Nous avons adapté l'algorithme d'apprentissage de règles CN2 \citep{clark1989cn2} à notre problème. Bien que CN2 ne réponde pas totalement à nos
attentes, puisqu'il génère un ensemble de règles auxquelles sont associées une classe, en modifiant les fonctions de score
 nous avons pu obtenir un résultat satisfaisant, c'est à dire un ensemble de règles couvrant un maximum de sacs positifs et
 un minimum de négatifs.

 \subsubsection{Induction d'une DNF par CN2}

 Le c\oe{}ur de l'algorithme reste inchangé, ce sont surtous les méthodes de score qui ont dû être modifiées.
 Puisque notre objectif est de construire des règles couvrant un maximum de positifs et un minimum de négatifs, 
 nous avons besoin d'un score mesurant la proportion des sacs positifs couvert par rapport au nombre total de sacs couvert,
 et non pas une mesure d'entropie, utilisée habituellement dans des problèmes d'apprentissage de concepts.
 
 Nous avons choisis le score \emph{tozero} \citep{blockeel2005multi} définis par $\frac{\#positifs}{\#total + k}$ avec $k$
 une valeur arbitraire. 

 \begin{algorithm}
    \Entree{R : Une structure de référence
    \newline N : Un ensemble de relations de voisinages
    }
    \Sortie{Une DNF positive}

    \BlankLine
    $P \leftarrow \emptyset$\;
    $E \leftarrow elements(R)$\;

    \Repeter{$c \neq null$ OU $score(c) < MIN\_SCORE$}{
        $c \leftarrow MeilleureClause(R, N, P)$\;
        $P \leftarrow [P, c]$\;
    }

    \Retour{P}

    \caption{CN2}
 \end{algorithm}

 \begin{algorithm}
     \Entree{R : Structure de référence
     \newline N : Ensemble de relation de voisinages
     \newline P : DNF partielle 
     }
     \Sortie{Une clause}

     \BlankLine
     $clauses \leftarrow \emptyset$\;
     $meilleureClause \leftarrow null$\;

     \Repeter{$clauses \neq \emptyset$}{
        $nouvellesClauses \leftarrow clauses \wedge N$\;
        Supprimer les clauses de $nouvellesClauses$ présentes dans $clauses$\;
        $clauses \leftarrow trier(nouvellesClauses)$\;

        \If{score($[P, meilleureClause]$) < score($[P, clauses[1]]$)}{
            $meilleureClause \leftarrow clauses[1]$\;
        }
     }

     \caption{MeilleureClause}
 \end{algorithm}

\subsection{Métriques}

Mes expérimentations ont été faites sur un jeu de données composé de 10 relations de voisinage sur un ensemble de 34 mots.
L'objectif était de construire une taxonomie.

La table \ref{tab:scores} représentes les scores obtenus avec différentes méthodes.
Mes résultats sont comparés à ceux de LPS \citep{DBLPCleuziouD15}.

On remarque que les scores de mon algorithme sont très dépendant de la fonction de score utilisée, et que le score
\emph{tozero} n'est pas suffisament sensible à la proportion de sacs négatifs couverts.

Les scores de LPS sont meilleurs puisqu'il utilise un algorithme génétique, et sur un tel exemple, on peut s'attendre à 
ce qu'il explore l'espace des DNF possible de façon exhaustive. On suppose donc que la fmesure obtenue par LPS est une représente 
la borne maximale sur ce jeu de données.


\begin{table}
    \centering
    \begin{tabular}{c|c|c|c}
        Méthode & Précision & Rappel & F-mesure \\
        \hline


        MIN\_SCORE = 0.2 & 0.093491124260355 & 0.766990291262136 & 0.166666666666667 \\
        Minimiser négatifs & 0.84 & 0.203883495145631 & 0.328125 \\
        LPS & 0 & 0 & 0
    \end{tabular}

    \caption{Score de précision et rappel obtenus}
    \label{tab:scores}
\end{table}


