\section{Espaces prétopologiques et apprentissage automatique}

L'objectif principal de mon travail est de traduire un ensemble de relations de voisinages ${\cal N}$ sur $E$ en une fonction
d'adhérence.

\subsection{Apprentissage d'une DNF}

Pour aprendre une fonction d'adhérence, l'algorithme d'apprentissage à besoin d'une connaissance partielle $R$ de l'espace qu'il doit apprendre.
Cette connaissance partielle peut être fournis, par exemple, par un expert du milieu cible.
A partir de cette connaissance partielle, il est possible de construire un ensemble d'apprentissage.
Cet ensemble est construit à partir des paires $(x, A) \in E \times {\cal P}(E)$.
A chacun de ces couples on associe un vecteur $(n_1, \ldots, n_k) \in \{0,1\}^k$ avec $k = |{\cal N}|$ et une étiquette de 
classe $x \in a(A)$.
Pour chaque élément $n_i$ du vecteur d'attribut on attribue la valeur $x \in d_{N_i}^+(A)$.

\begin{figure}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{R_simple.png}
        \caption*{$R$}
    \end{minipage}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{n1_simple.png}
        \caption*{$N_1$}
    \end{minipage}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{n2_simple.png}
        \caption*{$N_2$}
    \end{minipage}

    \caption{Structure de référence et relations de voisinages}
    \label{fig:relationsSimples}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{r|c|c|c|c}
        & $(x, A)$ & $N_1$ & $N_2$ & $x \in a(A)$\\
        \hline

        1 & $(voiture, \{vehicule\})$ & 1 & 0 & 1\\
        2 & $(camion, \{vehicule\})$ & 1 & 1 & 1\\

        3 & $(vehicule, \{voiture\})$ & 0 & 0 & 0\\
        4 & $(camion, \{voiture\})$ & 1 & 0 & 0\\
        
        5 & $(vehicule, \{camion\})$ & 0 & 0 & 0\\
        6 & $(voiture, \{camion\})$ & 0 & 1 & 0\\

        7 & $(camion, \{vehicule, voiture\})$ & 1 & 1 & 1\\
        8 & $(voiture, \{vehicule, camion\})$ & 1 & 1 & 1\\
        9 & $(vehicule, \{voiture, camion\})$ & 0 & 0 & 0\\
    \end{tabular}

    \caption{Ensemble d'apprentissage déduit des relations de la figure \ref{fig:relationsSimples}}
    \label{tab:ensembleApprentissageSimple}
\end{table}

Soit les relation de la figure \ref{fig:relationsSimples}. On obtient alors les fermés élémentaires suivants :
\begin{align*}
    F_R(vehicule) &= \{vehicule, voiture, camion\} \\
    F_R(voiture) &= \{voiture\} \\
    F_R(camion) &= \{camion\} \\
    \\
    F_{N_1}(vehicule) &= \{vehicule, voiture, camion\} \\
    F_{N_1}(voiture) &= \{voiture, camion\} \\
    F_{N_1}(camion) &= \{camion\} \\ 
    \\
    F_{N_2}(vehicule) &= \{vehicule, voiture, camion\} \\
    F_{N_2}(voiture) &= \{voiture\} \\
    F_{N_2}(camion) &= \{camion, voiture\}
\end{align*}

A partir de l'ensemble d'apprentissage de la table \ref{tab:ensembleApprentissageSimple}, on peut construire 
une fonction d'adhérence engendrant la structure $R$ : $a(A) = \{ x \in E \mid q_{N_1}(A, x) \land q_{N_2}(A, x) \}$.

Cependant, bien que cette solution permet d'obtenir les même fermés que la structure $R$, elle est imparfaite.
En effet, la première instance est rejettée par la fonction apprise, alors qu'elle est positive.
De plus, il est impossible de différencier les instances 1 et 4.

\subsection{L'apprentissage multi-instances à la rescousse}

Puisqu'un fermé élémentaire est calculé par application successive de la fonction d'adhérence, il existe plusieurs 
fonctions d'adhérence menant à un même fermé.

\begin{align*}
    a_1(\{vehicule\}) &= \{vehicule, camion\} \\
    a_1(\{vehicule, camion\}) &= \{vehicule, voiture, camion\} \\
    \\
    a_2(\{vehicule\}) &= \{vehicule, voiture, camion\}
\end{align*}

Bien que différentes, $a_1$ et $a_2$ mènent au même fermé élémentaire. Ces deux fonctions engendrent deux
espaces prétopologiques différents, mais ayant les même propriétés de structuration. Or, c'est cette 
propriété qui nous intéresse. Dans le cadre de notre travail, nous pouvons considérer que les espaces prétopologiques
engendrés par $a_1$ et $a_2$ sont \emph{similaires}.

Cela signifie que la seule contrainte que nous imposont à la fonction d'adhérence apprise se situe au niveau de ses
fermés élémentaires.
Les trois fonctions d'adhérence suivantes sont donc équivalentes :
\begin{align*}
    a_1(\{vehicule\}) &= \{vehicule, camion\} \\
    a_1(\{vehicule, camion\}) &= \{vehicule, voiture, camion\} \\
    \\
    a_2(\{vehicule\}) &= \{vehicule, voiture, camion\} \\
    \\
    a_3(\{vehicule\}) &= \{vehicule, voiture\} \\
    a_3(\{vehicule, voiture\}) &= \{vehicule, voiture, camion\}
\end{align*}

\subsubsection{Apprentissage multi-instances}

L'apprentissage multi-instance est une forme d'apprentissage supervisé dans lequel l'apprentissage ne se fais plusieurs
sur des instances individuelles, mais sur des ensembles d'instances, baptisés \emph{sacs d'instances}.
Les travaux sur ce domaine ont été initiés par \citeauthor{dietterich1997solving} alors qu'ils cherchaient un moyen
de déterminer si une molécule pouvait être utilisée dans la confection de médicaments.

Le principe de l'apprentissage multi-instance est qu'un élément peut avoir des formes, des comportements, différents.
Selon la manière dont on "regarde" l'élément en question, il peut ou non correspondre à ce que l'on cherche.
Si un élément possède $k$ formes distinctes ($k$ potentiellement très grand), et
qu'une seule d'entre elles correspond à une instances positive,
alors l'élément, le sac d'instances, doit être considéré positif.

Dans les relations de la figure \ref{fig:relationsSimples}, le fermé élémentaire de $\{vehicule\}$ peut 
être obtenu de trois façons différentes. Donc $\{vehicule\}$ engendrera trois sacs positifs.

\begin{table}
    \centering
    \begin{tabular}{c|c|c|c|c|c}
        SAC & $(x, A)$ & $N_1$ & $N_2$ & $x \in a(A)$ & $x \in F_R(A)$\\
        \hline \hline

        \multirow{2}{*}{1} & $(voiture, \{vehicule\})$ & 1 & 0 & 1 & \multirow{2}{*}{1} \\
        & $(camion, \{vehicule\})$ & 1 & 1 & 1 &\\
        \hline

        \multirow{2}{*}{2} & $(vehicule, \{voiture\})$ & 0 & 0 & 0 & \multirow{2}{*}{0}\\
        & $(camion, \{voiture\})$ & 1 & 0 & 0 &\\
        \hline

        \multirow{2}{*}{3} & $(vehicule, \{camion\})$ & 0 & 0 & 0 & \multirow{2}{*}{0}\\
        & $(voiture, \{camion\})$ & 0 & 1 & 0 &\\
        \hline

        4 & $(camion, \{vehicule, voiture\})$ & 1 & 1 & 1 & 1\\
        \hline

        5 & $(voiture, \{vehicule, camion\})$ & 1 & 1 & 1 & 1\\
        \hline

        6 & $(vehicule, \{voiture, camion\})$ & 0 & 0 & 0 & 0
    \end{tabular}

    \caption{Ensemble d'apprentissage déduit des relations de la figure \ref{fig:relationsSimples}}
    \label{tab:ensembleApprentissageSimpleMIL}
\end{table}

Le nouvel ensemble d'apprentissage, adapté pour l'apprentissage multi-instances est représenté 
dans la figure \ref{tab:ensembleApprentissageSimpleMIL}.
La DNF apprise précédémennt, $N_1 \land N_2$, permet à présent de différencier sans erreurs les sacs positifs et les sacs négatifs.
